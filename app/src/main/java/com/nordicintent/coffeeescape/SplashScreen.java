package com.nordicintent.coffeeescape;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 500;

    @BindView(R.id.ivCircle)
    ImageView ivCircle;
    @BindView(R.id.iv2015)
    ImageView iv2015;
    @BindView(R.id.ivEst)
    ImageView ivEst;
    @BindViews({R.id.ivStar1, R.id.ivStar2,
            R.id.ivStar3})
    List<ImageView> iv_blinking_stars;
    @BindViews({R.id.ivBean1, R.id.ivBean2, R.id.ivBean3})
    List<ImageView> ivBeans;

    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        //star animation scale animtion
        Animation animation1 = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.blink_star);
        animation1.setDuration(1000);
        iv_blinking_stars.get(0).startAnimation(animation1);

        Animation animation2 = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.blink_star);
        animation2.setDuration(1500);
        iv_blinking_stars.get(1).startAnimation(animation2);

        Animation animation3 = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.blink_star);
        animation3.setDuration(2000);
        iv_blinking_stars.get(2).startAnimation(animation3);

        //Bean animation drop
        TranslateAnimation animate3 = new TranslateAnimation(0, 0, -1500.0f, 0);
        animate3.setDuration(2000);
        ivBeans.get(0).startAnimation(animate3);

        TranslateAnimation animate4 = new TranslateAnimation(0, 0, -1500.0f, 0);
        animate4.setDuration(2400);
        ivBeans.get(1).startAnimation(animate4);

        TranslateAnimation animate5 = new TranslateAnimation(0, 0, -1500.0f, 0);
        animate5.setDuration(2800);
        ivBeans.get(2).startAnimation(animate5);

        //est and 2015 animation  slide in animation
        TranslateAnimation animate1 = new TranslateAnimation(1500.0f, 0, 0, 0);
        animate1.setDuration(3000);
        iv2015.startAnimation(animate1);

        TranslateAnimation animate2 = new TranslateAnimation(-1500.0f, 0, 0, 0);
        animate2.setDuration(3000);
        ivEst.startAnimation(animate2);

        animate2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = null;
                        i = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, SPLASH_TIME_OUT);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = null;
                i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);*/

        //star animation


       /* TranslateAnimation animation1 = new TranslateAnimation(0.0f, 0.0f, 1500.0f, 0.0f); // new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation1.setDuration(4000); // animation duration
        animation1.setFillAfter(false);
        iv2015 .startAnimation(animation1);*/


        //animating star android
      /*  for (int i = 0; i < iv_blinking_stars.size(); i++) {
            int random_duration = new Random().nextInt(400) + 200;
            animation = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.blink_star);
            animation.setDuration(random_duration);
            iv_blinking_stars.get(i).startAnimation(animation);
        }*/


    }


    @Override
    protected void onResume() {
        super.onResume();
        //slide in right




     /*   iv_blinking_stars.get(1).startAnimation(animation);
        iv_blinking_stars.get(2).startAnimation(animation);*/


    }


}
